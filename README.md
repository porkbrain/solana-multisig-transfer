A series of `solana` and `spl-token` CLI commands which showcases failing
transfer of tokens from one account to another using multisig signing.

Running following script which assumes that a local validator is running:

```bash
./multisig-transfer.sh
```

or save the stdout to a file with

```bash
./multisig-transfer.sh >>logs.txt 2>&1
```

You can run a local validator with:

```
solana-test-validator
```

# Resources

- https://docs.solana.com/offline-signing/durable-nonce
- https://docs.solana.com/implemented-proposals/durable-tx-nonces
- https://spl.solana.com/token#example-offline-signing-with-multisig
