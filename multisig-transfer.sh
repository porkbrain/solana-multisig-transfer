#!/bin/bash

#
# # Usage
# "./multisig-transfer.sh"
#

set -e

function gen_kp {
    # Generates a keypair and returns its path to stdout

    local name="${1}"
    local path="${dir}/${name}-kp.json"

    solana-keygen new --no-passphrase -o "${path}" &> /dev/null

    pubkey=$(solana-keygen pubkey ${path})
    echo "${name}: ${pubkey}" 1>&2

    echo "${path}"
}

# random prefix for all files
dir=$(cat /dev/urandom | tr -dc '[:alpha:]' | fold -w ${1:-5} | head -n 1)
dir="runs/${dir}"
mkdir -p "${dir}"

printf "\n\n----- pin solana -----\n\n"
SOLANA_VERSION="1.10.25"
solana --version 2>&1 1>/dev/null || sh -c "$(curl -sSfL https://release.solana.com/${SOLANA_VERSION}/install)"
solana --version | grep "${SOLANA_VERSION}" || solana-install init "${SOLANA_VERSION}"

printf "\n\n----- point CLI to local validator to it -----\n\n"
solana config set --url http://127.0.0.1:8899

printf "\n\n----- create a fee payer -----\n\n"
fp=$(gen_kp "fee-payer")
solana airdrop 100 -k "${fp}"

printf "\n\n----- prepare multisig authorities -----\n\n"
auth1_kp=$(gen_kp "auth1")
auth2_kp=$(gen_kp "auth2")
auth3_kp=$(gen_kp "auth3")

printf "\n\n----- create multisig -----\n\n"
multisig_kp=$(gen_kp "multisig")
spl-token create-multisig 2 "${auth1_kp}" "${auth2_kp}" "${auth3_kp}" \
    --fee-payer "${fp}" \
    --address-keypair "${multisig_kp}"

printf "\n\n----- create mint -----\n\n"
mint_kp=$(gen_kp "mint")
mint_auth_kp=$(gen_kp "mint-auth")
spl-token create-token "${mint_kp}" \
    --mint-authority "${mint_auth_kp}" \
    --fee-payer "${fp}"

printf "\n\n----- create source token account -----\n\n"
source_acc_kp=$(gen_kp "source-acc")
spl-token create-account "${mint_kp}" "${source_acc_kp}" \
    --owner "${multisig_kp}" \
    --fee-payer "${fp}"

printf "\n\n----- create target token account -----\n\n"
target_acc_kp=$(gen_kp "target-acc")
target_acc_owner=$(gen_kp "target-acc-owner")
spl-token create-account "${mint_kp}" "${target_acc_kp}" \
    --owner "${target_acc_owner}" \
    --fee-payer "${fp}"

printf "\n\n----- mint to source token account -----\n\n"
spl-token mint "${mint_kp}" 10 "${source_acc_kp}" \
    --mint-authority "${mint_auth_kp}" \
    --fee-payer "${fp}"

printf "\n\n----- create a nonce account -----\n\n"
nonce_kp=$(gen_kp "nonce")
nonce_auth_kp=$(gen_kp "nonce-auth")
solana airdrop 2 -k "${nonce_auth_kp}"
solana create-nonce-account "${nonce_kp}" 1 \
    --nonce-authority "${nonce_auth_kp}"
blockhash=$(solana nonce "${nonce_kp}")

printf "\n\n----- spl accounts info -----\n\n"
spl-token multisig-info "${multisig_kp}"
spl-token account-info --address "${source_acc_kp}"
spl-token account-info --address "${target_acc_kp}"

printf "\n\n----- multisig transfer -----\n\n"
transfer_cmd="spl-token transfer ${mint_kp} 10 ${target_acc_kp} \
    --from ${source_acc_kp} \
    --owner ${multisig_kp} \
    --multisig-signer ${auth1_kp} \
    --multisig-signer ${auth2_kp} \
    --multisig-signer ${auth3_kp} \
    --blockhash ${blockhash} \
    --fee-payer ${fp} \
    --nonce ${nonce_kp} \
    --nonce-authority ${nonce_auth_kp}"

# skip first 3 lines to get the list of signers in format
# pubkey1=signhash1
# pubkey2=signhash2
# ...
transfer_signers_lines=$( eval "${transfer_cmd} --mint-decimals 9  --sign-only" | tail -n +4 )
signers_flags=""
while IFS= read -r line; do
    signers_flags="${signers_flags} --signer ${line}"
done <<< "$transfer_signers_lines"

eval "${transfer_cmd} ${signers_flags}"
